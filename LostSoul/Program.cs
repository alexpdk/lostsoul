﻿using System;

namespace LostSoul {
#if WINDOWS||LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
		public static LSGame game;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            using (game = new LSGame())
                game.Run();
        }
    }
#endif
}
