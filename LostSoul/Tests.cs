﻿using Microsoft.Xna.Framework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LostSoul {
	[TestFixture]
	class Tests {
		static Ghost player;
		static GameScreen game;

		[SetUp]
		public static void SetUp() {
			game = GameLogic.game;
			player = GameLogic.player;
		}
		[Test, Order(1)]
		public static void CustomBoxTests() {
			var cb = new CustomBox(new Rectangle(100,100,200,100));
			Assert.AreEqual(cb.CenterX, 200);
			Assert.AreEqual(cb.CenterY, 150);

			cb.Scale = 2;
			Assert.That(new Rectangle(0, 50, 400, 200), Is.EqualTo(cb.Viewbox));

			cb.Cropbox = new Rectangle(0,0,50,200);
			Assert.That(new Rectangle(0, 50, 50, 150), Is.EqualTo(cb.Viewbox));
			Thread.Sleep(2000);
		}
		[Test, Order(3)]
		public static void CheckGraveTest() {
			player.AddActions(new StraightMotion(
				new Vector2(350, 450)
			));
			while(player.ActionGoing) Thread.Sleep(1000);

			var area = GameLogic.CheckGrave();
			while(player.ActionGoing) Thread.Sleep(1000);

			switch(area.discovery) {
				case DiscoveryType.Shield:
					Thread.Sleep(500);
					Assert.That(player.shieldNum, Is.EqualTo(1));
				break;
				case DiscoveryType.SpeedUp:
					Thread.Sleep(500);
					Assert.That(player.Accelerated);
				break;
				case DiscoveryType.TimeFreeze:
					Thread.Sleep(500);
					Assert.That(GameLogic.moon.Paused);
				break;
				case DiscoveryType.Victory:
					Thread.Sleep(2000);
					Assert.That(Program.game.currentState == LSGame.GameState.Victory);
					Assert.That(player.scores, Is.EqualTo(1000));
				return;
				case DiscoveryType.HandTrap:
					Assert.That(player.locked);
					// player and hand stop together
					Assert.That(!GameLogic.currentTrap.ActionGoing);
					//break trap
					while(player.locked) {
						player.SetMovement(1,-1);
						Thread.Sleep(500);
					}
					player.AddActions(new StraightMotion(
						player.Position + new Vector2(-1, 1)*(float)(Ghost.LockRadius*Math.Sqrt(2))
					));
					while(player.ActionGoing) Thread.Sleep(1000);
				break;
			}
			Assert.That(area.visited);
			while(game.MobileNum > 0) Thread.Sleep(1000);

			GameLogic.CheckGrave();
			while(player.ActionCount > 1) Thread.Sleep(500);
			if(area.discovery !=DiscoveryType.HandTrap) {
				// bonus cannot appear again
				Assert.That(game.MobileNum, Is.EqualTo(0));
			}else {
				Assert.That(game.MobileNum, Is.EqualTo(1));
				Assert.That(player.locked);
			}

			Thread.Sleep(2000);
		}
		[Test, Order(2)]
		public static void CheckPauseScreen() {
			var pos = player.Position;
			player.AddActions(new StraightMotion(
				player.Position + new Vector2(200, 0), interruptable: true
			));
			Program.game.currentState = LSGame.GameState.Paused;

			Thread.Sleep(2000);
			Assert.That(player.Position.X - pos.X < 2);

			player.InterruptActions();
			Assert.That(!player.ActionGoing);
			Program.game.currentState = LSGame.GameState.Playing;
		}
		[Test]
		public static void MoonFinishTest() {
			if(Program.game.currentState != LSGame.GameState.Playing)
				return;

			GameLogic.moon.Speed *= 20;
			while(!GameLogic.moon.MotionFinished) Thread.Sleep(1000);
			
			Assert.That(Program.game.currentState, Is.EqualTo(LSGame.GameState.LevelEnd));

			Thread.Sleep(2000);
		}
	}
}
