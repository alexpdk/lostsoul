﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LostSoul {

	public class Process {

		/// <summary>
		/// Presentage of animation completed
		/// </summary>
		protected double progress = 0;
		/// <summary>
		/// Total amount of work
		/// </summary>
		protected double total = 0;

		/// <summary> Function to call after process completion </summary>
		protected Action callback;
		/// <summary> Function to call during the process</summary>
		protected Action<double> action;

		private bool interruptable;
		private bool onMovement;

		/// <summary> Can player interrupt animation by pressing controls </summary>
		public virtual bool Interruptable {
			get { return interruptable; }
		}

		public virtual bool Complete {
			get { return progress >= 0.995;}
		}
		// Should process execution be controlled by object movement
		public virtual bool OnMovement{get {return onMovement; } }
		// Or by timer 
		public virtual bool OnFrame{get {return !onMovement; } }
		
		public double Progress {
			get { return progress; }
		}		

		public virtual void Begin(GameObject obj) {}

		public virtual Process ChangeProgress(double change) {
			progress += change / total;
			if(progress > 1) {
				progress = 1;
			}
			action?.Invoke(progress);
			return this;
		}
		public virtual void End() {
			// Stored inside 'Postponed' callback will be executed after
			// the finish of redrawing cycle
			if(callback != null) GameLogic.Postponed.Enqueue(callback);
		}
		public Process(Action<double> action, Action callback = null, double total = 0, bool interruptable = false, bool onMovement = false) {
			SetTotal(total);
			this.action = action;
			this.callback = callback;
			this.interruptable = interruptable;
			this.onMovement = onMovement;
		}
		public Process SetCallback(Action callback) {
			this.callback = callback;
			return this;
		}
		protected void SetTotal(double tVal) {
			if(tVal <= 0.001) {
				total = 0.001;
				progress = 1;
			}else {
				total = tVal;
				progress = 0;
			}
		}
	}

	public class CycleProcess: Process{
		public CycleProcess(double total, Action<double> action): base(total: total, action: action, onMovement: true) { }

		public override bool Complete {
			get { return false;}
		}

		public override Process ChangeProgress(double change) {
			progress += change / total;
			if(progress > 1) progress -= 1;
			action?.Invoke(progress);
			return this;
		}
	}

	public class StraightMotion: Process {

		private Vector2 targetPos;

		public StraightMotion(Vector2 targetPos, Action<double> action = null, Action callback=null, bool interruptable = false):
		base(action: action, callback: callback, interruptable: interruptable, onMovement: true)
		{
			this.targetPos = targetPos;
		}
		public override void Begin(GameObject obj) {
			var mob = obj as MobileObject;
			var route = mob.SetRouteTo(targetPos);
			SetTotal(route.Length());
		}
	}
	public class ScrollMotion: Process{
		private Vector2 distance;
		private double time;

		public ScrollMotion(Vector2 distance, double time): base(p=> { },  onMovement: true) {
			this.distance=distance;
			this.time=time;
		}
		public override void Begin(GameObject obj) {
			MobileObject mobile = obj as MobileObject;
			Vector2 targetPos = mobile.Position+distance;

			var route = (obj as MobileObject).SetRouteTo(targetPos);
			SetTotal(route.Length());
			mobile.speed=route.Length()/time;
		}
	}

	public class GameEffects {
		public static Process MakeBury(MobileObject obj, Rectangle cropbox, bool place=false) {
			var position = new Vector2(cropbox.Center.X, cropbox.Bottom - obj.Height / 2);
			if(place) obj.Position = position;
	
			return new StraightMotion(
				targetPos: position + new Vector2(0, obj.Height)
			);
		}
		public static Process MakeFade(MobileObject obj, Vector2 targetPos) {
			return new StraightMotion(
				targetPos: targetPos,
				action: progress=>obj.Opacity = (float)(1-progress)
			);
		}
		public static Process MakeFall(MobileObject obj, Rectangle bbox, MobileObject player=null, bool place=false, Action onHit=null) {
			var position = new Vector2(bbox.Center.X, bbox.Top - obj.Height / 2);
			if(place) obj.Position = position;

			return new StraightMotion(
				targetPos: new Vector2(bbox.Center.X, bbox.Bottom - obj.Height / 2),
				action: p=> { if(player!=null && obj.Bounds.Hitbox.Intersects(player.Bounds.Hitbox)) {
					onHit?.Invoke();
					onHit = null;
				}}
			);
		}

        /// <summary>
        /// Makes a star to track vector
        /// </summary>
        /// <param name="obj">star</param>
        /// <param name="bbox">opening area hitbox</param>
        /// <param name="vector">direction to follow</param>
        /// <returns></returns>
        public static Process MakeStarFall(MobileObject obj, Rectangle bbox, Vector2 vector)
        {
            obj.Position = new Vector2(bbox.Center.X, (bbox.Bottom - bbox.Height/2));
           
            return new StraightMotion(
                targetPos: vector,
                action: p => {}
            );
        }

        public static Process MakeJump(MobileObject obj, Vector2 direction, double jumpRadius) {
			var target = obj.Position + Vector2.Multiply(direction, (float)jumpRadius);
			return new StraightMotion(
				targetPos: target,
				interruptable: true
			);
		}

        public static Process MakeRise(MobileObject obj, Rectangle cropbox, bool place=false) {
			var position = new Vector2(cropbox.Center.X, cropbox.Bottom + obj.Height/2);
			if(place) obj.Position = position;
			
			return new StraightMotion(
				targetPos: position - new Vector2(0, obj.Height)
			);
		}
		public static Process MakeSlowDisappear(GameObject obj, double time) {
			return new Process(progress=> {
				obj.Opacity=(float)Math.Sqrt(1-progress);
			},
			total: time);
		}
		public static Process Pause(double duration) {
			return new Process(total: duration, 
				action: null
			);
		}
	}
}
