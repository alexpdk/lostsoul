﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Media;

namespace LostSoul {

	public class GameLogic {
		public const double HandPauseDuration = 1.2;
		public const double OpenAnimationDuration = 1.0;
		public const double ShieldDisplayTime = 3.0;
		public const double SpeedUpDuration = 3.0;
		public const double TimeFreezeDuration = 3.0;

		public const double TotalTime = 60.0;

		public const double HandSpeed = 40;

		public const int TotalLifeNum = 3;

		public static GameScreen game;
		public static Ghost player;
		public static Moon moon;
		public static Random Rnd = new Random();

		public static bool LevelCompleted{
			get { return victory || player.heartsNum == 0 || moon.MotionFinished;}
		}
		public static bool PlayerDead {
			get { return player.heartsNum == 0;}
		}
		// Used to execute callbacks after completed Draw cycle.
		// Required if animation callback tries to modify 'characters'
		public static Queue<Action> Postponed = new Queue<Action>();

		// Hand, that currently trapped character or null
		public static MobileObject currentTrap = null;
		private static CheckArea trapArea = null;
		// Current chance to break trap
		private static double breakChance = 0.0;

		private static bool victory = false;
		
		public static CheckArea CheckGrave() {
			// check tombstones nearby, return center
			var area = game.map.GetSearchArea(player.Bounds, player.TurnedRight);
			if(area == null) return null;

			var playerDst = new Vector2(area.Opening.Hitbox.Center.X, area.Opening.Viewbox.Center.Y);

			if(player.locked && area.check == CheckType.Bury) {
				player.SetMovement(null, -1);
				return null;
			}
			if(!player.ActionGoing) {
				switch(area.check) {
					case CheckType.Bury:
						var hitbox = area.Opening.Hitbox;
						playerDst.Y += (area.Opening.Hitbox.Height - player.Height)*0.5f; // just touch the ground

						player.AddActions(
							new StraightMotion(playerDst).SetCallback(()=> {
								game.stopScroll = true;
								player.IntersectionEnabled = false; //to avoid automatic change of player speed
								player.Bounds.Cropbox = area.Opening.Hitbox;
							}),
							GameEffects.MakeBury(player, hitbox).SetCallback(()=> {
								player.AnimateAreaVisit(area, time:0.8);
								ShowCheckResult(area);
							}),
							GameEffects.MakeRise(player, hitbox).SetCallback(()=> {
								game.stopScroll = false;
								player.IntersectionEnabled = true;
								player.Bounds.ResetCrop();
							})
						);
					break;
					case CheckType.Open:
						player.AddActions(
							new StraightMotion(playerDst, callback: ()=>
								player.AnimateAreaVisit(area, OpenAnimationDuration)
							),
							GameEffects.Pause((area.visited) ? 0 : OpenAnimationDuration)
							.SetCallback(()=> 
								ShowCheckResult(area)
							)
						);
					break;
				}
			}
			return area;
		}
		public static MobileObject CreateBonus(CheckArea area, Texture2D bonusTexture, Action onRise) {
			var bonus = new MobileObject(bonusTexture, height: 30, speed: HandSpeed);
			game.AddMobile(bonus);
			var box = Rectangle.Intersect(area.Opening.Hitbox, area.Opening.Viewbox);

			var iconDst = box.Center.ToVector2();
			iconDst.Y -= box.Height;
			box.Inflate(Math.Max(bonus.Width - box.Width, 0), 0);
			bonus.Bounds.Cropbox = box;

			bonus.AddActions(
				GameEffects.MakeRise(bonus, box, place: true).SetCallback(()=> {
					onRise(); bonus.Bounds.ResetCrop();
				}),
				GameEffects.MakeFade(bonus, iconDst).SetCallback(()=> 
					game.RemoveMobile(bonus)
				)
			);

            player.scores += 100;
			return bonus;
		}
		public static MobileObject CreateFallTrap(CheckArea area) {
			var trap = new MobileObject(Resources.fallTrap, height: 25, speed: HandSpeed*4);			
			game.AddMobile(trap);

			trap.Bounds.Cropbox = area.Opening.Hitbox;
			trap.AddActions(
				GameEffects.MakeFall(trap, area.Opening.Hitbox, player, place:true, onHit:()=> {
					player.InterruptActions();

					if(player.TryUseShield()) {
						player.MakeJump(direction: new Vector2(
							(player.TurnedRight) ? 1 : -1, -1
 						));
					}else {
						player.AddActions(
							GameEffects.MakeFall(player, area.Opening.Hitbox),
							GameEffects.Pause(0.5));
						player.heartsNum--;
					}
				}),
				GameEffects.MakeSlowDisappear(trap, 1.0).SetCallback(()=>game.RemoveMobile(trap))	
			);
			return trap;
		}

        /// <summary>
        /// creates winning stars
        /// </summary>
        /// <param name="area"></param>
        public static void CreateStars(CheckArea area)
        {
            var inner_box = Rectangle.Intersect(area.Opening.Hitbox, area.Opening.Viewbox);
            var star = new MobileObject(Resources.star, height: 30, speed: 250);
            var star1 = new MobileObject(Resources.star, height: 30, speed: 250);
            var star2 = new MobileObject(Resources.star, height: 30, speed: 250);
            game.AddMobile(star);
            game.AddMobile(star1);
            game.AddMobile(star2);

            star.AddActions(
                GameEffects.MakeStarFall(star, inner_box, new Vector2(inner_box.Center.X, inner_box.Bottom - 100)),
                GameEffects.MakeSlowDisappear(star, 2.0).SetCallback(() => game.RemoveMobile(star))
            );
            star1.AddActions(
                GameEffects.MakeStarFall(star1, inner_box, new Vector2(inner_box.Center.X+40, inner_box.Bottom -70)),
                GameEffects.MakeSlowDisappear(star1, 2.0).SetCallback(() => game.RemoveMobile(star))
            );
            star2.AddActions(
                GameEffects.MakeStarFall(star2, inner_box, new Vector2(inner_box.Center.X-40, inner_box.Bottom - 70)),
                GameEffects.MakeSlowDisappear(star2, 2.0).SetCallback(() => game.RemoveMobile(star))
            );
        }

        public static MobileObject CreateHand(CheckArea area, bool playerTrapped) {
			var hand = new MobileObject(Resources.handTexture, height: 40, speed: HandSpeed);			
			game.AddMobile(hand);

			hand.Bounds.Cropbox = area.Opening.Hitbox;
			hand.AddActions(GameEffects.MakeRise(hand, area.Opening.Hitbox, place:true));

			if(playerTrapped) {
				currentTrap = hand;
				trapArea = area;
				player.heartsNum--;
				player.locked = true;
				breakChance = 0; 
			}else {
				player.MakeJump(direction: new Vector2(
					(player.TurnedRight) ? 1 : -1, -1
 				));
				hand.AddActions(
					GameEffects.Pause(HandPauseDuration),
					GameEffects.MakeBury(hand, area.Opening.Hitbox).SetCallback(()=> game.RemoveMobile(hand))
				);
			}
			return hand;
		}
		public static void ExecuteCallbacks() {
			// Execute callbacks, stored by finished animations.
			// Dequeue removes and returns first callback.
			while(Postponed.Count > 0) Postponed.Dequeue()();
		}
		public static void ResetVictory() {
			victory = false;
		}
		public static void ShowCheckResult(CheckArea place) {

			if(place.visited) {
				Console.WriteLine(place);
			}
			switch(place.discovery) {
				case DiscoveryType.HandTrap:
					Resources.zombieSound.Play();
					var handLock = !player.Accelerated && !player.TryUseShield();
					var hand = //(place.horizontal) ? CreateTurnedHand(place.box, playerTrapped: handLock)
							CreateHand(place, playerTrapped: handLock);
					if(handLock)
						player.speed = hand.speed * player.Bounds.Original.Height
							/ hand.Bounds.Original.Height;
				break;
				case DiscoveryType.FallTrap:
					Resources.fallSound.Play();
					CreateFallTrap(place);
				break;
				case DiscoveryType.SpeedUp:
					if(!place.visited) Resources.bonusSound.Play();
					if(!place.visited) CreateBonus(place, Resources.speedBonus, ()=>{
						player.speedUpTime = SpeedUpDuration;
					});
				break;
				case DiscoveryType.Shield:
					if(!place.visited) Resources.bonusSound.Play();
					if(!place.visited) CreateBonus(place, Resources.shieldBonus, ()=>{
						player.shieldNum++;
					});
				break;
				case DiscoveryType.TimeFreeze:
					if(!place.visited) Resources.bonusSound.Play();
					if(!place.visited) CreateBonus(place, Resources.timeFreeze, ()=>{
						moon.Paused = true;
						moon.PauseTime = TimeFreezeDuration;
					});
				break;
				case DiscoveryType.Victory:
                    //win sound
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Resources.winSound);

                    CreateStars(place);

                    player.AddActions(
						GameEffects.Pause(duration: 4.0)
						.SetCallback(()=>victory=true)
					);
                    player.scores += 1000;
				break;
			}
			place.visited = true;
		}
		public static bool TryBreakTrap() {
			if(Rnd.NextDouble() <= breakChance) {
				
				var ct = currentTrap;
				ct.AddActions(
					GameEffects.Pause(HandPauseDuration / 3),
					GameEffects.MakeBury(ct, trapArea.Opening.Hitbox).SetCallback(()=> game.RemoveMobile(ct))
				);
				currentTrap = null;
				trapArea = null;
				player.locked = false;
				return true;
			}else {
				breakChance = Math.Sqrt(breakChance+0.1);
				return false;
			}
		}
	}
}
