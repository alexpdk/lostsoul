﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LostSoul {

	public class Ghost : MobileObject{

		private const int LeftDirection = 0;
		private const int RightDirection = 1;

		public const double FreeSpeed = 200;
		public const double ObstacleSpeed = 90;

		private const double SpeedUp = 2.0;     // how much character speed is increased
		public const double LockRadius = 20.0; // how much character can move away when locked to point
		private const double JumpRadius = LockRadius*3;  

		private const double SpriteMotionDelta = FreeSpeed;

		public bool intersects = true; // intersection with ground to slow character checked
		public bool locked = false;    // returns to start position after each motion
		public bool rotated = true;    // changes direction according to route

		public int heartsNum = GameLogic.TotalLifeNum;
		public MobileObject shield;
		public int shieldNum = 0;
		public double speedUpTime = 0;

        public int scores;

		private Vector2 minPosition, maxPosition;
		private bool right_dir = false;
		
		public bool Accelerated {
			get { return speedUpTime > 0; }
		}
		public bool IntersectionEnabled {
			get { return intersects; }
			set { intersects = value; }
		}
		public bool TurnedRight {
			get { return right_dir; }
		}

		public Ghost(Texture2D image, float xPos, float yPos) : base(image, height:40, speed:0, spriteWidth: 70, spriteHeight:60){
			var orig = Bounds.Original;
			Bounds.SetHitbox(origX: orig.X+3, origY: orig.Y+2, origWidth: 31, origHeight: 36);
			Position = new Vector2(xPos, yPos);

			SetSpriteAnimation(relativeSpeed: 1.2);
			SetDirection(right: true);
		}
		public void AnimateAreaVisit(CheckArea area, double time) {
			if(!area.visited) AddState(new Process(
				action: p=>area.opacity = (float)p,
				total: time
			));
			//area.visited = true;
		}
		public void BeginFreeMotion() {
			speed = FreeSpeed * ((speedUpTime > 0) ? SpeedUp : 1);
		}
		public void BeginMotionThroughObstacle() {
			speed = ObstacleSpeed * ((speedUpTime > 0) ? SpeedUp : 1);
		}
		public void ChangeDirection(bool right) {
			var orig = Bounds.Original;
			var hb = Bounds.OrigHitbox;

			Bounds.SetHitbox(origX: orig.Right - hb.Right + orig.X);
			Bounds.ShiftCenter(stagePoint: Bounds.Hitbox.Center);
			Bounds.MoveTo(Position.X, Position.Y);

			SetSprite((right) ? RightDirection : LeftDirection);
		}
		public override void Draw(SpriteBatch batch) {
			
			if(shield != null)
				shield.Draw(batch);
			base.Draw(batch);
		}
		public void MakeJump(Vector2 direction) {
			AddActions(GameEffects.MakeJump(this, direction, JumpRadius));
		}
		public void MakeJump() {
			AddActions(GameEffects.MakeJump(this, movement, JumpRadius));
		}
		/// <summary>
		/// Method to control movement from keyboard / other input device
		/// </summary>
		/// <param name="mX">Direction by X axis, null if not specified</param>
		/// <param name="mY">Direction by Y axis, null if not specified</param>
		public void SetMovement(float? mX, float? mY) {
			// Some animations can't be interrupted from keyboard
			if(ActionGoing && InterruptActions() == false) return;
			if(mX.HasValue) {
				movement.X = mX.Value;
				if(movement.X != 0 && rotated) SetDirection(right: movement.X > 0);
			}
			if(mY.HasValue) movement.Y = mY.Value;
			if(movement.Length() > 0) movement.Normalize();

			if(locked && movement.Length()>0) {
				var lockPoint = Position;
				var shift = Vector2.Multiply(movement, (float)LockRadius);

				if(GameLogic.TryBreakTrap()) {
					movement.Y = -1;
					MakeJump();
				} else {
					AddActions(
						new StraightMotion(lockPoint + shift, interruptable: false),
							//.SetCallback(()=>rotated = false),
						new StraightMotion(lockPoint, interruptable: false)
							.SetCallback(()=>rotated = true)
					);
					rotated = false;
				}
			}
			else hasRoute = false;
		}
		public void SetFurthestPosition(Vector2 corner) {

			var hb = Bounds.Hitbox;
			minPosition = new Vector2((float)Bounds.CenterX - hb.X, (float)Bounds.CenterY - hb.Y);
			maxPosition =-new Vector2(hb.Right - (float)Bounds.CenterX, hb.Bottom - (float)Bounds.CenterY)
				+ corner;
		}
		public void SetDirection(bool right) {
			if(right && !right_dir) {
				ChangeDirection(right: true);
			}
			else if(!right && right_dir) {
				ChangeDirection(right: false);
			}
			right_dir = right;
		}
		public override Vector2 SetRouteTo(Vector2 targetPos) {
			var route = base.SetRouteTo(targetPos);
			 if((Math.Abs(movement.X) > 0.05) && rotated) SetDirection(right: movement.X > 0);
			return route;
		}
		public override void Update(double time) {
			// TODO make it one of animations
			if(speedUpTime > 0) {
				speedUpTime -= time;
				if(speedUpTime < 0) {
					speedUpTime = 0;
					speed /= SpeedUp;
				}
			} 
			base.Update(time);

			if(shield != null) {
				shield.Position = Position;
				shield.Update(time);
			}
		}
		public bool TryUseShield() {
			if(shield != null)
				return true;
			else if(shieldNum > 0) {
				shield = new MobileObject(Resources.shieldBonus, Bounds.OrigHitbox.Height);
				shield.AddActions(
					GameEffects.MakeSlowDisappear(shield, GameLogic.ShieldDisplayTime)
					.SetCallback(()=>shield=null)
				);
				shield.Bounds.Scale = 1.5;
				shieldNum--;
				return true;
			}
			return false;
		}
		public void ValidatePosition() {
			Position = Vector2.Clamp(Position, minPosition, maxPosition);
		}
	}
}
