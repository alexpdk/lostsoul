﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LostSoul
{
    public abstract class GameComponent
    {

        public abstract Matrix DrawTransform
        {
            get;
        }
        public virtual bool Paused
        {
            get; set;
        }
        public double PauseTime = 0;

        public abstract void Draw(SpriteBatch batch);
        public virtual void Init() { }
		public virtual void Reload() { }
        public abstract void Update(double elapsedTime);
    }

    public class BasicScreen : GameComponent
    {
        private String Text;
        private int textWidth;
        public float opacity = 1.0f;

        public override Matrix DrawTransform
        {
            get { return Matrix.Identity; }
        }
        public BasicScreen(params String[] text)
        {
            Text = String.Join("\n", text);
        }
        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(Resources.background,
                destinationRectangle: Resources.WindowBounds,
                sourceRectangle: Resources.WindowBounds,
                origin: null,
                color: Color.White * opacity
            );
            batch.DrawString(Resources.quill, Text,
                new Vector2((Resources.WindowBounds.Width - textWidth) / 2, 50),
            Color.White);
        }
        public override void Init()
        {
            textWidth = (int)Resources.quill.MeasureString(Text).X;
        }
        public BasicScreen SetOpacity(float val)
        {
            opacity = val;
            return this;
        }
        public override void Update(double elapsedTime) { }
    }

    class PlayerPanel : GameComponent
    {
        public const int HEIGHT = 50;
        
        public override Matrix DrawTransform
        {
            get { return Matrix.Identity; }
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(Resources.panelTexture, new Rectangle(
                0, 0, Resources.WindowBounds.Width, PlayerPanel.HEIGHT),
            Color.White);

            batch.DrawString(Resources.quill, "Score: " + GameLogic.player.scores,
                                        new Vector2(700, 10), Color.White);

            var x = 30;
            for (int i = 0; i < GameLogic.TotalLifeNum; i++)
            {
                batch.Draw(
                    (i < GameLogic.player.heartsNum) ? Resources.activeHeart : Resources.inactiveHeart,
                    new Rectangle(x, 10, 30, 30 * 72 / 95),
                Color.White);
                x += 35;
            }
            for (int i = 0; i < GameLogic.player.shieldNum; i++)
            {
                batch.Draw(Resources.shieldBonus, new Rectangle(x, 5, 30, 30), Color.White);
                x += 30;
            }
            if (GameLogic.moon.Paused)
            {
                batch.Draw(Resources.timeFreeze, new Rectangle(x, 5, 30, 30), Color.White);
                x += 30;
            }
            if (GameLogic.player.Accelerated)
            {
                batch.Draw(Resources.speedBonus, new Rectangle(x, 5, 30, 30), Color.White);
                x += 30;
            }
        }

        public override void Update(double elapsedTime)
        {
            // scores++;
        }
    }

    public class Moon : GameComponent
    {

        private const float Scale = 0.2f;
        public static Matrix DisplayScale = Matrix.CreateScale(Scale);

        private MobileObject moon;
        private bool finished = false;
		private double time = GameLogic.TotalTime;

        public override Matrix DrawTransform
        {
            get { return DisplayScale; }
        }
        public bool MotionFinished
        {
            get { return finished; }
        }
		public double Speed {
			get { return moon.speed; }
			set { moon.speed = value; }
		}
        public override void Init()
        {
            moon = new MobileObject(Resources.moonTexture, height: (int)(120 / Scale));
            moon.Position = new Vector2(moon.Width / 2, 175 / Scale);

            var moonMotion = new ScrollMotion(
                distance: new Vector2(Resources.WindowBounds.Width / Scale - moon.Width, moon.Height / 2),
                time: this.time)
            .SetCallback(() => finished = true);

            moon.AddActions(moonMotion);
            GameLogic.moon = this;
			finished = false;
        }
        public override void Draw(SpriteBatch batch)
        {
            moon.Draw(batch);
        }
		public override void Reload() {
			Init();
			Paused = false;
		}
		public void SetMotionTime(int? time) {
			this.time = (time != null) ? time.Value : GameLogic.TotalTime;
		}
        public override void Update(double time)
        {
            moon.Update(time);
        }
    }

    public class GameScreen : GameComponent
    {

        public static Color Background = Color.Black; // new Color(3,0,34);

        // List of drawable game characters(not tile elements)
        private List<GameObject> characters = new List<GameObject>();

		public int levelNum = 0;

        public LevelMap map;

        // List of mobile game characters
        private List<MobileObject> mobile = new List<MobileObject>();

        private double passedTime = 0.0;

        public Ghost player;

        public bool stopScroll = false;

        // Translation matrix for `viewport scroll`
        private Matrix translation = Matrix.Identity;

        public override Matrix DrawTransform
        {
            get { return translation; }
        }
		public int MobileNum {
			get { return mobile.Count; }
		}
        public void AddMobile(MobileObject obj)
        {
            characters.Add(obj);
            mobile.Add(obj);
        }
        public void RemoveMobile(MobileObject obj)
        {
            characters.Remove(obj);
            mobile.Remove(obj);
        }
        public void ControlMovement(float? mX, float? mY)
        {
            player.SetMovement(mX, mY);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
			if(levelNum == 0) {
				spriteBatch.DrawString(Resources.quill, "Use arrows or WASD\n to move",
					 new Vector2(100, 200), Color.White);

				spriteBatch.DrawString(Resources.quill, "Press <Space> to check tomb",
					 new Vector2(300, 600), Color.White); 

				spriteBatch.DrawString(Resources.quill, "Press <P> to pause",
					 new Vector2(600, 200), Color.White);

				spriteBatch.DrawString(Resources.quill, "Sometimes you should also\n check uncommon objects:",
					 new Vector2(1180, 100), Color.White);
			}else {
				spriteBatch.DrawString(Resources.quill, "Larger level, explore it",
					 new Vector2(100, 2500), Color.White);
			}
           

            map.Draw(spriteBatch);
            player.Draw(spriteBatch);
            // draw other game characters)
            foreach (var ch in characters)
            {
                ch.Draw(spriteBatch);
            }
            GameLogic.ExecuteCallbacks();
        }
        public override void Init()
        {
            map = new LevelMap(levelNum);
            map.LoadContent();

			player = new Ghost(Resources.ghostSpritesheet, xPos: map.startPos.X, yPos: map.startPos.Y);
            player.Opacity = 0.7f;
            player.SetFurthestPosition(new Vector2(map.totalSize.Width, map.totalSize.Height));
			player.AddActions(new StraightMotion(map.targetPos, interruptable: true));

            GameLogic.player = player;
            GameLogic.game = this;
        }
		public override void Reload() {
			Init();
			stopScroll = false;
			characters.Clear();
			mobile.Clear();
			passedTime = 0.0;
		}
        public override void Update(double time)
        {

            //Console.WriteLine(1 / time);
            passedTime += time;

            if (player.IntersectionEnabled)
            {
                if (map.CheckCollision(player.Bounds.Hitbox))
                {
                    player.BeginMotionThroughObstacle();
                }
                else player.BeginFreeMotion();
            }
            player.Update(time);
            player.ValidatePosition(); // check, player is not out of stage bounds
            foreach (var m in mobile)
            {
                m.Update(time);
            }
            var bounds = Resources.WindowBounds;
            var hitbox = player.Bounds.Hitbox;
            // scroll viewport to center player
            if (!stopScroll) Matrix.CreateTranslation(
                 MathHelper.Clamp(
                         -hitbox.Center.X + bounds.Width / 2,
                         -map.totalSize.Width + bounds.Width, 0),
                 MathHelper.Clamp(
                         -hitbox.Bottom + bounds.Height / 2,
                         -map.totalSize.Height + bounds.Height, 0),
                 0, out translation);
        }
    }
}
