﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LostSoul {
	public abstract class GameObject {

		public float Opacity = 1.0f;

		public CustomBox Bounds;    // object bounding box on the stage
		public CustomBox Source;    // object source box from spritesheet
		protected Texture2D image;  // plain image or spritesheet

		protected Queue<Process> actions = new Queue<Process>();
		protected List<Process> states = new List<Process>();
		protected Process spriteAnimation;

		public int ActionCount {
			get { return actions.Count; }
		}
		public bool ActionGoing {
			get { return actions.Count > 0;}
		}
		public int Width {
			get { return (int)(Bounds.Original.Width * Bounds.Scale); }
		}
		public int Height {
			get { return (int)(Bounds.Original.Height * Bounds.Scale); }
		}

		public GameObject(Texture2D image, int height, int spriteWidth = 0, int spriteHeight = 0) {
			this.image = image;
			Source = new CustomBox(
				width:  (spriteWidth > 0) ? spriteWidth : image.Width,
				height: (spriteHeight> 0) ? spriteHeight: image.Height
			);
			Bounds = new CustomBox(
				width:  height * Source.Ratio,
				height: height
			);
		}
		public void AddActions(params Process[] list) {
			foreach(var anim in list) {
				actions.Enqueue(anim);
			}
			actions.Peek().Begin(this);
		}
		public void AddState(Process state) {
			states.Add(state);
			state.Begin(this);
		}
		public virtual void Draw(SpriteBatch batch) {
			batch.Draw(image, Bounds.Viewbox, Source.ApplyViewbox(Bounds), Color.White*Opacity);
		}
		public virtual bool InterruptActions() {
			if(actions.Count > 0 && actions.Peek().Interruptable) {
			 	Bounds.ResetCrop();
				Source.ResetCrop();
				actions.Clear();
				return true;
			}
			return false;
		}
		//public void NextSprite() {
		//	Source.CenterX = (Source.CenterX + Source.Original.Width) % image.Width;
		//}

		/// <summary> Specify sprite in spritesheet </summary>
		/// <param name="line"> Spritesheet line</param>
		/// <param name="num"> Sprite number in line, first by default</param>
		public void SetSprite(int line, int num = 0) {
			Source.MoveTo(
				Source.Original.Width * (num+0.5),
				Source.Original.Height * (line+0.5)
			);
		}
	}


	public class MobileObject : GameObject{

		// just Vector2 wrapper for Bounds.CenterX, CenterY
		private Vector2 position = new Vector2();
		protected Vector2 movement = new Vector2(); // normalized movement direction of ghost

		protected Vector2 route; // way to pass to reach a specified point
		protected bool hasRoute; // Whether it's used for motion

		public double speed;

		public Vector2 Position {
			get { return position; }
			set {
				position = value;
				Bounds.MoveTo(position.X, position.Y);
			}
		}
		
		public MobileObject(Texture2D image, int height, double speed=0, int spriteWidth=0, int spriteHeight=0):
		base(image, height, spriteWidth, spriteHeight) {
			this.speed = speed;
		}	
		public override bool InterruptActions() {
			bool canInterrupt = base.InterruptActions();
			if(canInterrupt) {
				// Here object movement should be cleared, else it can continue moving in direction,
				// specified by last animation
				movement = Vector2.Zero;
			};
			return canInterrupt;
		}
		public void SetSpriteAnimation(double relativeSpeed) {

			if(spriteAnimation != null) {
				states.Remove(spriteAnimation);
			}
			var spriteRel = (double)image.Width / Source.Original.Width;
			spriteAnimation = new CycleProcess(
				total: image.Width * relativeSpeed,
				action: p=> {
					Source.CenterX = (Math.Floor(p * spriteRel)+0.5) * Source.Original.Width;
				});
			states.Add(spriteAnimation);
		}

		public virtual void Update(double time) {
			if(hasRoute) {
				time = Math.Min(time, route.Length() / speed);
			}
			var step = movement * (float)(speed*time);
			Position += step;

			foreach(var state in states) {
				state.ChangeProgress((state.OnMovement) ? step.Length() : time); 
			}
			states.RemoveAll(state=>state.Complete);

			if(hasRoute) {
				if(route.Length() < 0.1) {
					hasRoute = false;
					movement = Vector2.Zero;
					step = route;
				}
				route -= step;
			}

			if(actions.Count > 0) {
				var action = actions.Peek();
				action.ChangeProgress((action.OnMovement) ? step.Length() : time);
				if(action.Complete) {
					actions.Dequeue().End();
					if(actions.Count > 0) actions.Peek().Begin(this); 
				}
			}
		}
		/// <summary>Specify linear route. </summary>
		/// <param name="targetPos">Destination point for object bounding box center</param>
		public virtual Vector2 SetRouteTo(Vector2 targetPos) {
			// Debug.WriteLine("TP {0}", targetPos);
			route = targetPos - position;
			// Because Vector2 is struct, movement assigned with separate copy
			movement = route;
			if(movement.Length() > 0) movement.Normalize();
			hasRoute = true;

			return route;
		}
	}
}
