﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LostSoul {
	[SetUpFixture]
	class TestSetup {
		[OneTimeSetUp]
		public static void SetUp() {
			Task.Run(()=>Program.Main());
			while(Program.game == null || Program.game.currentState != LSGame.GameState.Playing) {
				Thread.Sleep(1000);
			}
		}
		[OneTimeTearDown]
		public static void TearDown() {
			Thread.Sleep(2000);
			Program.game.Exit();
			Thread.Sleep(2000);
		}
	}
}
