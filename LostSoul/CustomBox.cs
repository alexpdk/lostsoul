﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LostSoul {
	public class CustomBox {
		private Rectangle viewbox;
		private Rectangle hitbox; // contained inside viewbox 
		private Rectangle cropbox;
		private bool cropped = false;

		public double CenterX, CenterY;
		public double Scale = 1.0;

		public Rectangle Cropbox {
			get { return cropbox; }
			set {
				cropbox = value;
				cropped = true;
			}
		}
		public Rectangle Hitbox {
			get {
				var hb = Transform(hitbox);
				return (cropped) ? Rectangle.Intersect(hb, cropbox) : hb;
			}
		}
		public Rectangle Original {
			get { return viewbox;  }
		}
		public Rectangle OrigHitbox {
			get { return hitbox; }
		}
		/// <summary> original viewbox width/height </summary>
		public double OrigRatio {
			get {
				return (double) viewbox.Width / viewbox.Height;
			}
		}
		/// <summary> viewbox width/height </summary>
		public double Ratio {
			get {
				var v = (cropped) ? Rectangle.Intersect(viewbox, cropbox) : viewbox;
				return (double) v.Width / v.Height;
			} 
		}
		public Rectangle Viewbox {
			get {
				var vb = Transform(viewbox);
				return (cropped) ? Rectangle.Intersect(vb, cropbox) : vb;
			}
		}
		public Rectangle ApplyViewbox(CustomBox src) {
			var Vb = src.Viewbox;
			var k = (double)viewbox.Width / src.viewbox.Width;
			return new Rectangle(
				(int)Math.Max((Vb.X - Vb.Center.X) * k + CenterX, CenterX - viewbox.Width/2),
				(int)Math.Max((Vb.Y - src.CenterY) * k + CenterY, CenterY - viewbox.Height/2),
				(int)Math.Min(Vb.Width * k, viewbox.Width),
				(int)Math.Min(Vb.Height * k, viewbox.Height)
			);
		}
		public CustomBox(Rectangle viewbox, Rectangle hitbox) {
			this.viewbox = viewbox;
			this.hitbox = hitbox;
			cropbox = viewbox;
			ShiftCenter(hitbox.Center);
		}
		public CustomBox(Rectangle viewbox) : this(viewbox, viewbox){ }
		public CustomBox(int width, int height) : this(new Rectangle(0,0,width,height)) { }
		public CustomBox(double width, double height) : this(new Rectangle(0,0,(int)width, (int)height)) { }
		public void MoveTo(double X, double Y) {
			CenterX = X;
			CenterY = Y;
		}
		public void ResetCrop() {
			cropped = false;
			cropbox = viewbox;
		}
		public void SetHitbox(double? origX=null, double? origY=null, double? origWidth=null, double? origHeight=null) {
			if(origX != null) hitbox.X = (int)origX.Value;
			if(origY != null) hitbox.Y = (int)origY.Value;
			if(origWidth != null)  hitbox.Width = (int)origWidth.Value;
			if(origHeight != null) hitbox.Height = (int)origHeight.Value;
		}
		public void ShiftCenter(Point stagePoint) {
			var dX = (int)((CenterX - stagePoint.X) / Scale);
			var dY = (int)((CenterY - stagePoint.Y) / Scale);
			viewbox.Offset(dX, dY);
			hitbox .Offset(dX, dY);
			CenterX = stagePoint.X * Scale;
			CenterY = stagePoint.Y * Scale;
		}
		public Rectangle Transform(Rectangle box) {
			return new Rectangle(
				(int)(box.X * Scale + CenterX), 
				(int)(box.Y * Scale + CenterY), 
				(int)(box.Width * Scale),
				(int)(box.Height* Scale));
		}
	}
}
