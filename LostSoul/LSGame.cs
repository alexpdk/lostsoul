﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TiledSharp;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using System.Threading;

namespace LostSoul
{

	public class LSGame : Game
	{
        public enum GameState
		{
			Startmenu,
			Loading,
			Playing,
			Paused,
			LevelEnd,
			Victory
		}
		public GameState currentState;
		private bool isLoading = false;
		private Thread backgroundThread;

		bool isMouseVisible = true;
		MouseState mouseState;
		MouseState previousMouseState;

		private Texture2D startButton;
		private Texture2D exitButton;
		private Texture2D pauseButton;
		private Texture2D resumeButton;
        private Texture2D reviveButton;
        private Texture2D loadingScreen;

		private Vector2 startButtonPosition;
		private Vector2 exitButtonPosition;
		private Vector2 resumeButtonPosition;
        private Vector2 reviveButtonPosition;

		private const String header = "Lost Soul";
		private static int headerTextWidth;
        private const String introText =
@"Playing as a ghost, you should find your
body under one of tombstones till the
morning. Use wisely bonuses you can
discover in tombs and beware of traps.";
		private static int introTextWidth;
		private const String resumeText =
@"Press the button or <Space> to resume
                         the game";
		private static int resumeTextWidth;
		private const String endText =
@"      You are dead.
This time forever.


No, <R>evive again!
       Ok, <E>xit";
		private static int endTextWidth;
        private static String loadingText =
@"             Loading game... Please wait a while...";

		private String victoryText;
		private static int victoryTextWidth;

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		GameScreen gameScreen;

		BasicScreen startScreen = new BasicScreen(
			"Playing as a ghost, you should find your",
			"body under one of tombstones till the",
			"morning. Use wisely bonuses you can",
			"discover in tombs and beware of traps.",
			"",
			"Press <Space> to begin."
		);
		BasicScreen pauseScreen = new BasicScreen(
			"   Game is paused.", "Press <P> to resume."
		).SetOpacity(0.75f);

		BasicScreen endScreen = new BasicScreen(
			"       You died.", "That time forever."
		);
		BasicScreen victoryScreen = new BasicScreen(
			"Found yourself!"
		);
		PlayerPanel playerPanel = new PlayerPanel();
		Moon moon = new Moon();



		private List<GameComponent> openComponents = new List<GameComponent>();

		// previous and current keyboard state
		// TODO search for keyboard events
		private KeyboardState pState, kState;

		public LSGame()
		{
			graphics = new GraphicsDeviceManager(this);
			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferHeight = 550;
			graphics.PreferredBackBufferWidth = 900;
			Content.RootDirectory = "Content";
		}
		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			//GraphicsDevice.Clear((current == State.Run) ? GameScreen.Background : Color.Black);
			GraphicsDevice.Clear(GameScreen.Background);

			if (currentState == GameState.Startmenu)
			{
				headerTextWidth = (int)Resources.quillHeader.MeasureString(header).X;
				introTextWidth = (int)Resources.quill.MeasureString(introText).X;

				spriteBatch.Begin();
				spriteBatch.DrawString(Resources.quillHeader, header,
										new Vector2((Resources.WindowBounds.Width - headerTextWidth) / 2, 50), Color.White);

				spriteBatch.DrawString(Resources.quill, introText,
										new Vector2((Resources.WindowBounds.Width - introTextWidth) / 2, 180), Color.White);


				spriteBatch.Draw(startButton, startButtonPosition, Color.White);
				spriteBatch.Draw(exitButton, exitButtonPosition, Color.White);
				spriteBatch.End();
			}

			if (currentState == GameState.Loading)
			{
				spriteBatch.Begin();
				spriteBatch.DrawString(Resources.quill, loadingText,
					 new Vector2((GraphicsDevice.Viewport.Width/2)-
								   (loadingScreen.Width/2), (GraphicsDevice.Viewport.Height/2)-
								   (loadingScreen.Height/2)), Color.White);
				spriteBatch.End();
			}

			if (currentState == GameState.Playing || currentState == GameState.Paused)
			{
				spriteBatch.Begin();
				spriteBatch.Draw(Resources.skyTexture, new Rectangle(
					0, 0, Resources.WindowBounds.Width,  Resources.WindowBounds.Height),
				Color.White);
				spriteBatch.End();

				foreach (var comp in openComponents)
				{
					spriteBatch.Begin(
						SpriteSortMode.Deferred,
						BlendState.AlphaBlend, SamplerState.LinearWrap, null, null, null,
						comp.DrawTransform
					);
					comp.Draw(spriteBatch);
					spriteBatch.End();
				}
			}

			if (currentState == GameState.Paused)
			{
				resumeTextWidth = (int)Resources.quill.MeasureString(introText).X;

				spriteBatch.Begin();
				spriteBatch.Draw(Resources.background,
					destinationRectangle: Resources.WindowBounds,
					sourceRectangle: Resources.WindowBounds,
					origin: null,
					color: Color.White * 0.75f
				);
				spriteBatch.DrawString(Resources.quill, resumeText,
										new Vector2((Resources.WindowBounds.Width - resumeTextWidth) / 2, 50), Color.White);
				spriteBatch.Draw(resumeButton, resumeButtonPosition, Color.White);
                spriteBatch.Draw(exitButton, exitButtonPosition, Color.White);
                spriteBatch.End();
			}

			if(currentState == GameState.LevelEnd) {

                endTextWidth = (int)Resources.quill.MeasureString(endText).X;
				
				spriteBatch.Begin();
                //that rapist face moon appears when you loose now
                spriteBatch.Draw(Resources.moonTexture, new Vector2(50, Resources.WindowBounds.Height - 380), Color.White);

                spriteBatch.DrawString(Resources.quill, endText,
										new Vector2((Resources.WindowBounds.Width - endTextWidth) / 2, 50), Color.White);
                spriteBatch.Draw(reviveButton, reviveButtonPosition, Color.White);
                spriteBatch.Draw(exitButton, exitButtonPosition, Color.White);
                spriteBatch.End();
			}
			if(currentState == GameState.Victory) {
                victoryText = "Found yourself! Hurray!\n You gained " + GameLogic.player.scores + " score points";
                victoryTextWidth = (int)Resources.quill.MeasureString(victoryText).X;

				spriteBatch.Begin();
				spriteBatch.DrawString(Resources.quill, victoryText,
										new Vector2((Resources.WindowBounds.Width -victoryTextWidth) / 2, 50), Color.White);
                if(gameScreen.levelNum < Resources.LevelNum-1)
					spriteBatch.Draw(reviveButton, reviveButtonPosition, Color.White);
                spriteBatch.Draw(exitButton, exitButtonPosition, Color.White);
                spriteBatch.End();
			}

			base.Draw(gameTime);
		}
		protected void HandleInput()
		{

			kState = Keyboard.GetState();
			if (kState.IsKeyDown(Keys.Escape)) Exit();

			float? mX = null, mY = null;

			if (KeyPressed(Keys.Left) || KeyPressed(Keys.A))
			{
				mX = -1;
			}
			else if (KeyPressed(Keys.Right) || KeyPressed(Keys.D))
			{
				mX = 1;
			}
			else if (KeyReleased(Keys.Left) && !kState.IsKeyDown(Keys.Right) ||
					KeyReleased(Keys.Right) && !kState.IsKeyDown(Keys.Left) ||
                    KeyReleased(Keys.A) && !kState.IsKeyDown(Keys.D) ||
                    KeyReleased(Keys.D) && !kState.IsKeyDown(Keys.A))
                mX = 0;

            if (KeyPressed(Keys.Up) || KeyPressed(Keys.W))
            {
                mY = -1;
            }
            else if (KeyPressed(Keys.Down) || KeyPressed(Keys.S))
            {
                mY = 1;
            }
            else if (KeyReleased(Keys.Up) || KeyReleased(Keys.Down) || KeyReleased(Keys.W) || KeyReleased(Keys.S))
            {
                mY = 0;
            }

			if (mX.HasValue || mY.HasValue)
			{
				if (currentState == GameState.Playing) gameScreen.ControlMovement(mX, mY);
			}

			if (KeyPressed(Keys.P))
			{
				switch (currentState)
				{
					case GameState.Playing:
						currentState = GameState.Paused;
						break;
					case GameState.Paused:
						currentState = GameState.Playing;
						break;
				}
			}
			if(KeyPressed(Keys.R)) {
				if(currentState == GameState.LevelEnd) {
					ReloadLevel();
				}
				else if(currentState == GameState.Victory && gameScreen.levelNum < Resources.LevelNum-1) {
					NextLevel();
				}
			}
			if(KeyPressed(Keys.E)) {
				if(currentState != GameState.Playing) Exit();
			}

			if (KeyPressed(Keys.Space))
			{
				switch (currentState)
				{
					case GameState.Startmenu:
						currentState = GameState.Loading;
						isLoading = false;
						break;
					case GameState.Playing:
						GameLogic.CheckGrave();
						break;
					case GameState.Paused:
						currentState = GameState.Playing;
						break;
				}
			}

			pState = kState;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			//- Debug.WriteLine(this.Window.ClientBounds);
			//- pState = Keyboard.GetState();

			IsMouseVisible = true;

			startButtonPosition = new Vector2((GraphicsDevice.Viewport.Width / 2) - 65, 400);
			exitButtonPosition = new Vector2((GraphicsDevice.Viewport.Width / 2) - 65, 450);

			// set the gamestate to start menu
			currentState = GameState.Startmenu;

			// get the mousestate
			mouseState = Mouse.GetState();
			previousMouseState = mouseState;

			base.Initialize();
		}
		protected bool KeyPressed(Keys keyCode)
		{
			return kState.IsKeyDown(keyCode) && pState.IsKeyUp(keyCode);
		}
		protected bool KeyReleased(Keys keyCode)
		{
			return kState.IsKeyUp(keyCode) && pState.IsKeyDown(keyCode);
		}
		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			Resources.Init(Window.ClientBounds, Content);
			//- gameScreen.Init();	pauseScreen.Init();
			//- playerPanel.Init();	startScreen.Init(); endScreen.Init(); moon.Init();

			moon.Init();

			startButton = Content.Load<Texture2D>(@"start");
			exitButton = Content.Load<Texture2D>(@"exit");
			loadingScreen = Content.Load<Texture2D>(@"loading");
		}
		void LoadGame()
		{
			gameScreen = new GameScreen();
			gameScreen.Init();
			playerPanel.Init();
			moon.Init();

			openComponents = new List<GameComponent>();
			openComponents.Add(moon);
			openComponents.Add(gameScreen);
			openComponents.Add(playerPanel);

			spriteBatch = new SpriteBatch(GraphicsDevice);
			pauseButton = Content.Load<Texture2D>(@"pause");
			resumeButton = Content.Load<Texture2D>(@"resume");
            reviveButton = Content.Load<Texture2D>(@"revive");

            resumeButtonPosition = new Vector2((GraphicsDevice.Viewport.Width / 2) - (resumeButton.Width / 2), 400);
            reviveButtonPosition = new Vector2((GraphicsDevice.Viewport.Width / 2) - (reviveButton.Width / 2), 400);

            // since this will go to fast for this demo's purpose, wait for 1 second
            Thread.Sleep(1000);

			// start playing
			currentState = GameState.Playing;
			isLoading = true;
		}
		void NextLevel() {
			gameScreen.levelNum++;
			ReloadLevel();
		}
		void MouseClick(int x, int y)
		{
			// creates a rectangle of 10x10 around the place where the mouse was clicked
			Rectangle mouseClickRect = new Rectangle(x, y, 10, 10);

			// check the startmenu
			if (currentState == GameState.Startmenu)
			{
				Rectangle startButtonRect = new Rectangle((int)startButtonPosition.X,
														  (int)startButtonPosition.Y, 130, 40);
				Rectangle exitButtonRect = new Rectangle((int)exitButtonPosition.X,
														 (int)exitButtonPosition.Y, 130, 40);

				// check the clicked position
				if (mouseClickRect.Intersects(startButtonRect))
				{
					currentState = GameState.Loading;
					isLoading = false;
				}
				else if (mouseClickRect.Intersects(exitButtonRect))
				{
					Exit();
				}
			}

            if (currentState == GameState.Victory || currentState == GameState.LevelEnd)
            {
                Rectangle exitButtonRect = new Rectangle((int)exitButtonPosition.X,
                                                         (int)exitButtonPosition.Y, 130, 40);
                Rectangle reviveButtonRect = new Rectangle((int)reviveButtonPosition.X,
                                                           (int)reviveButtonPosition.Y, 130, 40);

                if (mouseClickRect.Intersects(exitButtonRect))
                {
                    Exit();
                }
                else if (mouseClickRect.Intersects(reviveButtonRect))
                {
                    if(currentState == GameState.Victory) NextLevel(); else ReloadLevel();
                }

            }

            // check the pause button
            if (currentState == GameState.Playing)
			{
				Rectangle pauseButtonRect = new Rectangle(0, 0, 70, 70);

				if (mouseClickRect.Intersects(pauseButtonRect))
				{
					currentState = GameState.Paused;
				}
			}

			// check the resume button
			if (currentState == GameState.Paused)
			{
				Rectangle resumeButtonRect = new Rectangle((int)resumeButtonPosition.X,
														   (int)resumeButtonPosition.Y, 100, 20);
                Rectangle exitButtonRect = new Rectangle((int)exitButtonPosition.X,
                                                         (int)exitButtonPosition.Y, 130, 40);

                if (mouseClickRect.Intersects(resumeButtonRect))
				{
					currentState = GameState.Playing;
				}
                else if (mouseClickRect.Intersects(exitButtonRect))
                {
                    Exit();
                }

            }
		}
		void ReloadLevel() {
            
            //restart melody
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(Resources.song);

			gameScreen.Reload();
			moon.SetMotionTime(gameScreen.map.GetTime());
			moon.Reload();
			GameLogic.ResetVictory();
			currentState = GameState.Playing;
		}


		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// double time = gameTime.ElapsedGameTime.TotalSeconds;

			// load the game when needed

			// isLoading bool is to prevent the LoadGame method from being called 60 times a second
			if (currentState == GameState.Loading && !isLoading)
			{
				// set the background thread
				backgroundThread = new Thread(LoadGame);
				isLoading = true;

				// start the background thread
				backgroundThread.Start();
			}

			HandleInput();
			//- if(GameLogic.LevelCompleted) SetState(State.LevelEnd);
			if (currentState == GameState.Playing)
			{
				double time = gameTime.ElapsedGameTime.TotalSeconds;

				HandleInput();
				if(GameLogic.LevelCompleted) {
					currentState = (GameLogic.PlayerDead || GameLogic.moon.MotionFinished) ? GameState.LevelEnd : GameState.Victory;
                    
                    if (currentState!=GameState.Victory) {
                        //loose sound
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Resources.loseSound);
                    }
                    
                }
				foreach (var comp in openComponents)
				{
					if (!comp.Paused)
					{
						comp.Update(time);
					}
					else if (comp.PauseTime > 0)
					{
						comp.PauseTime -= time;
						if (comp.PauseTime <= 0)
						{
							comp.PauseTime = 0;
							comp.Paused = false;
						}
					}
				}
			}

			if (currentState == GameState.Paused)
			{
				HandleInput();
			}

			// wait for the mouseclick
			mouseState = Mouse.GetState();
			if (previousMouseState.LeftButton == ButtonState.Pressed &&
				mouseState.LeftButton == ButtonState.Released)
			{
				MouseClick(mouseState.X, mouseState.Y);
			}
			previousMouseState = mouseState;

            if (currentState == GameState.Playing && isLoading)
			{
				LoadGame();
				isLoading = false;
			}

			base.Update(gameTime);
		}
	}

	public class Resources
	{
		public const int LevelNum = 2;

		public static Texture2D activeHeart;
		public static Texture2D background;
        public static Texture2D fallTrap;
        public static Texture2D star;
        public static Texture2D shieldBonus;
		public static Texture2D speedBonus;
		public static Texture2D ghostSpritesheet;
		public static Texture2D handTexture;
		public static Texture2D handTurned;
		public static Texture2D inactiveHeart;
		public static Texture2D moonTexture;
		public static Texture2D panelTexture;
		public static Texture2D skyTexture;
		public static Texture2D timeFreeze;

		public static SpriteFont quill;
		public static SpriteFont quillHeader;

		public static TmxMap[] maps = new TmxMap[LevelNum];
		public static Texture2D[] tilesets = new Texture2D[LevelNum];

		public static Rectangle WindowBounds;

		public static SoundEffect bonusSound;
		public static SoundEffect zombieSound;
		public static SoundEffect fallSound;
		public static Song winSound;
		public static Song loseSound;
		public static Song song;

		public static void Init(Rectangle bounds, ContentManager content)
		{
			WindowBounds = bounds;
			WindowBounds.X = 0;
			WindowBounds.Y = 0;

			activeHeart = content.Load<Texture2D>("ActiveHeart");
			background = content.Load<Texture2D>("black");
			fallTrap = content.Load<Texture2D>("box");
			speedBonus = content.Load<Texture2D>("SpeedBonus");
			shieldBonus = content.Load<Texture2D>("ShieldBonus");
			ghostSpritesheet = content.Load<Texture2D>("ghost-anim");
			handTexture = content.Load<Texture2D>("HandEnemy");
			handTurned = content.Load<Texture2D>("HandTurned");
			inactiveHeart = content.Load<Texture2D>("InactiveHeart");
			moonTexture = content.Load<Texture2D>("moonbeta");
			panelTexture = content.Load<Texture2D>("panel");
			skyTexture = content.Load<Texture2D>("sky");
            timeFreeze = content.Load<Texture2D>("TimeFreezeBonus");
            star = content.Load<Texture2D>("star");

            bonusSound = content.Load<SoundEffect>("SoundFX/bonus");
			zombieSound = content.Load<SoundEffect>("SoundFX/zombie");
			fallSound = content.Load<SoundEffect>("SoundFX/fall_trap");
			winSound = content.Load<Song>("SoundFX/win");
			loseSound = content.Load<Song>("SoundFX/evil laugh");
			song = content.Load<Song>("SoundFX/spooky_island");  // Put the name of your song here instead of "song_title"
			MediaPlayer.IsRepeating = true;
			MediaPlayer.Play(song);

			for(int i=0; i<LevelNum; i++) {
				var path = "Content/Levels2/level"+(i+1)+".tmx";
				path = "C:/CviDocuments/Proj/LostSoul/LostSoul/bin/Windows/x86/Debug/"+path;
				maps[i] = new TmxMap(path);
				tilesets[i] = content.Load<Texture2D>(maps[i].Tilesets[0].Name.ToString());
			}

			quill = content.Load<SpriteFont>("Quill");
			quillHeader = content.Load<SpriteFont>("QuillHeader");
		}
	}
}
