﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using TiledSharp;

namespace LostSoul {
	public class TileSet {

		public int cols, rows, tileWidth, tileHeight;
		private Texture2D set;

		public Texture2D Image {
			get{ return set; }
		}

		public TileSet(Texture2D texture, TmxMap map) {
			this.set = texture;
			tileWidth = map.Tilesets[0].TileWidth;
			tileHeight = map.Tilesets[0].TileHeight;
			
			cols = texture.Width / tileWidth;
			rows = texture.Height / tileHeight;
		}

		public void SetTile(int tileID, ref Rectangle tile) {
			tile.X = (tileID-1) % cols * tileWidth;
			tile.Y = (tileID-1) / cols * tileHeight;
			tile.Width = tileWidth;
			tile.Height = tileHeight;
		}
	}

	enum CollisionType {
		Empty, Full, Sector
	}
	public enum CheckType {
		Bury, Open
	}
	public enum DiscoveryType {
		SpeedUp, TimeFreeze, Shield, HandTrap, FallTrap, Victory
	}

	public class CheckArea {
		public static int TypeNumber = Enum.GetValues(typeof(DiscoveryType)).Length;

		public CustomBox Opening;  // Viewbox contains total area, which can be checked, Hitbox used to center
			                       // character and display trap
		public int layerNum;       // Map layer, that contains tiles to display checked state

		public DiscoveryType discovery;
		public bool visited = false;
		public float opacity = 0.0f;
		public CheckType check;

		public int startX, startY, endX, endY;
	}
 
	public class LevelMap {
		const int RIGHT = 1;
		const int TOP = 2;
		const int TYPE = 4;

		public const int DefaultCheckedDisplayLayer = 3;
		public const int Size = 25; //displayed tile size in px

		public const double TrapChance = 0.45; //chance, that a loaded check area will contain trap 
		public const double ChanceDelta = 0.02; //current trap chance changed during generation to avoid extreme values 

		public int levelNum;
		public Vector2 startPos, targetPos;

		private List<CheckArea> checkAreas = new List<CheckArea>();
		private Dictionary<int, Rectangle> openings = new Dictionary<int, Rectangle>(); 

		private int[] collisions;
		private double currentChance = TrapChance;

		/// <summary>
		/// 3-dimensional array - level map
		/// indices specify x/y/depth
		/// </summary>
		private int[,,] map;

		/// <summary>
		/// Rectangle, used for drawing tiles in proper place and with unified size
		/// </summary>
		private Rectangle srcTile, dstTile;

		private TileSet tileset;
		private double tileScale;

		/// <summary>
		/// Bounding rectangle of curent level
		/// </summary>
		public Rectangle totalSize;
		
		//Map size measured in tile number
		private int width;
		private int height;

		public LevelMap(int levelNum) {
			srcTile = new Rectangle(0,0,Size,Size);
			dstTile = new Rectangle(0,0,Size,Size);
			this.levelNum = levelNum;
		}
		/// <param name="bbox">Player bounding box</param>
		/// <returns>Does player collide with tiles considered as obstacles</returns>
		public bool CheckCollision(Rectangle bbox) { 
			int iStart = bbox.X / Size;
			int iEnd = Math.Min((bbox.X+bbox.Width) / Size, width-1);
			int jStart = bbox.Y / Size;
			int jEnd = Math.Min((bbox.Y+bbox.Height) / Size, height-1);

			for(int i = iStart; i <= iEnd; i++) {
				for(int j = jStart; j <= jEnd; j++) {
					var code = collisions[map[i,j,0]];
					switch((CollisionType)(code / TYPE)) {
						case CollisionType.Empty: break;
						case CollisionType.Full: return true;
						case CollisionType.Sector:
							dstTile.X = i*Size;
							dstTile.Y = j*Size;
							if(CheckSectorCollision(bbox, dstTile, (code & TOP) > 0, (code & RIGHT) > 0)) {
								Console.WriteLine("CC");
								return true;
							}
						break;
					}
				}
			} 
			return false;
		}
		public bool CheckSectorCollision(Rectangle bbox, Rectangle tile, bool isTop, bool isRight) {
			var dX = (isRight) ? bbox.Left - tile.Left : tile.Right - bbox.Right;
			var dY = (isTop) ? tile.Bottom - bbox.Bottom : bbox.Top - tile.Top;
			return (dX <= 0) || (dY <= 0) || (Math.Sqrt(dX*dX+dY*dY) <= Size);
			//return res;  
		} 
		public CheckArea CreateArea(TmxObject obj) {
			var area = new CheckArea();

			var bbox = GetAreaRect(obj);
			var openingId = GetIntProperty(obj, "opening");
			area.Opening = (openingId != null) ? new CustomBox(bbox, openings[openingId.Value]) : new CustomBox(bbox);

			var dLayer = GetIntProperty(obj, "display_layer");
			area.layerNum = (dLayer != null) ? dLayer.Value-1 : DefaultCheckedDisplayLayer;

			var check = GetStringProperty(obj, "check");
			area.check = (check != null && check.Equals("open")) ? CheckType.Open : CheckType.Bury;

			var fall = GetBoolProperty(obj, "fall");
			var fallTrap = (fall != null) && fall.Value;


			if(GameLogic.Rnd.NextDouble() < currentChance) {
				area.discovery = (DiscoveryType)((fallTrap) ? CheckArea.TypeNumber-2 : CheckArea.TypeNumber-3);
				currentChance -= ChanceDelta;
			}else {
				area.discovery = (DiscoveryType) GameLogic.Rnd.Next(CheckArea.TypeNumber-3);
				currentChance += ChanceDelta;
			}

			area.startX = GetTileIndex(bbox.Left, large:false);
			area.startY = GetTileIndex(bbox.Top, large:false);
			area.endX = GetTileIndex(bbox.Right, large:true) - 1;
			area.endY = GetTileIndex(bbox.Bottom, large:true) - 1;
			return area;
		}

		public void Draw(SpriteBatch batch) {
			for(int k = 0; k < DefaultCheckedDisplayLayer; k++) {
				for(int i = 0; i < width; i++) {
					for(int j = 0 ; j < height; j++) {
						 if(map[i,j,k] != 0) {
							dstTile.X = i*Size;
							dstTile.Y = j*Size;
							tileset.SetTile(map[i,j,k], ref srcTile);
							batch.Draw(tileset.Image, dstTile, srcTile, Color.White);
						}
					}
				}
			}
			foreach(var area in checkAreas) {
				for(int i = area.startX; i <= area.endX; i++) {
					for(int j = area.startY; j <= area.endY; j++) {
						if(map[i,j,area.layerNum] != 0) {
							dstTile.X = i*Size;
							dstTile.Y = j*Size;
							tileset.SetTile(map[i,j,area.layerNum], ref srcTile);
							batch.Draw(tileset.Image, dstTile, srcTile, Color.White * area.opacity);
						}
					}
				}
			}
		}
		private void FillMap() {
			var map = Resources.maps[levelNum];
			this.map = new int[width, height, map.Layers.Count];

			for(var i = 0; i < map.Layers.Count; i++) {
				for (var j = 0; j < map.Layers[i].Tiles.Count; j++) {
					int gid = map.Layers[i].Tiles[j].Gid;
					var x = j % width;
					var y = j / width;
					this.map[x, y, i]=gid;
				}
			}
		}
		/// <returns> Rectangle, described by TmxObject </returns>
		private Rectangle GetAreaRect(TmxObject obj) {
			return new Rectangle(
				(int)(obj.X*tileScale), (int)(obj.Y*tileScale), (int)(obj.Width*tileScale), (int)(obj.Height*tileScale)
			);
		}
		private bool? GetBoolProperty(TmxObject obj, String property) {
			if(obj.Properties.ContainsKey(property)) {
				return Convert.ToBoolean(obj.Properties[property]);
			}
			return null;
		}
		private int? GetIntProperty(TmxObject obj, String property) {
			if(obj.Properties.ContainsKey(property)) {
				return Convert.ToInt32(obj.Properties[property]);
			}
			return null;
		}
		private string GetStringProperty(TmxObject obj, String property) {
			if(obj.Properties.ContainsKey(property)) {
				return obj.Properties[property];
			}
			return null;
		}
		public int? GetTime() {
			var map = Resources.maps[levelNum];
			return (map.Properties.ContainsKey("time")) ? (int?)Convert.ToInt32(map.Properties["time"]) : null ;
		}
		private int GetTileIndex(int pos, bool large) {

			var rel = (double)pos / Size;
			var round = Math.Round(rel);
			if(rel - round < -0.1 && !large) {
				return (int)round-1;
			}else if(rel - round > 0.1 && large) {
				return (int)round+1;
			}else {
				return (int)round;
			}
		}

		/// <param name="bbox">Player bounding box</param>
		/// <returns>Rectangular area to inspect intersecting player or null</returns>
		public CheckArea GetSearchArea(CustomBox bbox, bool rightDir) {
			foreach(var area in checkAreas) {
				if(area.Opening.Viewbox.Contains(bbox.Hitbox.Center))
					//Intersects(bbox.Hitbox) && 
					//	((bbox.Viewbox.Center.X <= area.box.Center.X) == rightDir))
					return area;
			}
			return null;
		}
		public void LoadContent() {
			var map = Resources.maps[levelNum];
            tileset = new TileSet(Resources.tilesets[levelNum], map);
			tileScale = (double)(Size) / tileset.tileWidth;

            width = map.Width;
			height = map.Height;
			FillMap();
			ReadSpawnPoint();

			if(map.ObjectGroups.Count > 1) {
				foreach(var obj in map.ObjectGroups[1].Objects) {
					openings.Add(GetIntProperty(obj, "cid").Value, GetAreaRect(obj));
				}
			}
			foreach(var obj in map.ObjectGroups[0].Objects) {
				checkAreas.Add(CreateArea(obj));
			}
			checkAreas.Sort((a1, a2)=>a1.layerNum.CompareTo(a2.layerNum));

            var victoryArea = checkAreas[GameLogic.Rnd.Next(checkAreas.Count)];
            victoryArea.discovery = DiscoveryType.Victory;


            collisions = new int[map.Tilesets[0].TileCount.Value];
			collisions[1] = TOP+TYPE*(int)CollisionType.Sector;
			collisions[2] = TYPE*(int)CollisionType.Full;
			collisions[3] = TOP+RIGHT+TYPE*(int)CollisionType.Sector;

			collisions[1+tileset.cols] = TYPE*(int)CollisionType.Full;
			collisions[2+tileset.cols] = TYPE*(int)CollisionType.Full;
			collisions[3+tileset.cols] = TYPE*(int)CollisionType.Full;

			collisions[1+tileset.cols*2] = TYPE*(int)CollisionType.Sector;
			collisions[2+tileset.cols*2] = TYPE*(int)CollisionType.Full;
			collisions[3+tileset.cols*2] = RIGHT+TYPE*(int)CollisionType.Sector;
			
			totalSize = new Rectangle(0,0,width*Size, height*Size);
		}
		public void ReadSpawnPoint() {
			var map = Resources.maps[levelNum];
			startPos = new Vector2(
				Convert.ToInt32(map.Properties["x0"]),
				Convert.ToInt32(map.Properties["y0"])
			);
			targetPos = new Vector2(
				Convert.ToInt32(map.Properties["x1"]),
				Convert.ToInt32(map.Properties["y1"])
			);
		}
	}
}
