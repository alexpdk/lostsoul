# Lost Soul #
Simple game about a ghost, looking for its missing body.

![LostSoul.jpg](https://bitbucket.org/repo/RGB8eE/images/550880965-LostSoul.jpg)

First release can be obtained [here](https://drive.google.com/open?id=0B6OOVYVadw5XT21ubWtDNDJoV2c).